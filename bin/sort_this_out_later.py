
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                 description="kill or list processes by name")
    parser.add_argument("-l", "--list",
                        help="list matching processes without killing",
                        action="store_true")
    parser.add_argument("-v", "--verbose",
                        help="give verbose output", action="store_true")
    parser.add_argument(
                "-a", metavar="--attempts-string",
               default="TERM:0:0.01:0.1:0.3:1,INT:0.1:1,HUP:1,KILL:1,KILL:3",
               help="comma-separated list of messages to send"
                    + ", each starting with the type of message"
                    + ", then waiting for the following durations to continue"
                    + " (default: %(default)s)")
    parser.add_argument("-L", "--loop",
                        help="loop last attempt", action="store_true")
    parser.add_argument("pattern", nargs="*",
                        help="string(s) to match processes to"
                             + ", use quotes to pass wildcards")
    args = parser.parse_args()
    superkill(args.list, args.verbose, args.a, args.loop, args.pattern)
